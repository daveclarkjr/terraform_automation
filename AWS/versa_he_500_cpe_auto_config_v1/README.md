This Terraform Template is intended to Automate and bringing up Versa Head End setup on AWS in one touch mode.
It will bring up 2 instances of Versa Director, 2 analytics node, DC service VNF , DR service VNF & Versa Controller's in different regions.

# Pre-requisites for using this template:

- **Terraform Install:** To Download & Install Terraform, refer Link "www.terraform.io/downloads.html"
- **Versa Head End Images:** Image available and added in AWS in .ami format for:
  - Versa Director
  - Versa Controller/service VNF
  - Versa Analytics


# Usage:

- Download all the files in PC where Terraform is installed. It is recommended that place all the files in folder as terraform will store the state file for this environment once it is applied.
- Go to the folder "versa_he_500_cpe_auto_config_v1" where all the required files are placed.

- Use command `terraform init` to initialize. it will download necessary terraform plugins required to run this template.
- Then use command `terraform plan` to plan the deployment. It will show the plan regarding all the resources being provisioned as part of this template.
- At last use command `terraform apply` to apply this plan in action for deployment. It will start deploying all the resource on Azure.

The following figure illustrates the redundant (high availability) headend topology created by the Terraform template(s).

![500_CPE_TOPOLOGY.png](./500_CPE_TOPOLOGY.png)

It will require below files to run it.


 |--versa_he_500_cpe_auto_config_v1
 | |--output.tf
 | |--main.tf
 | |--terraform.tfvars
 | |--variable.tf
 | |--versa_he_500_cpe_multiregion_dc
 | | |--output.tf
 | | |--main.tf
 | | |--analytics.sh
 | | |--variable.tf
 | | |--dc_dr_network_config_gen.yaml
 | | |--master_director.sh
 | |--versa_he_500_cpe_multiregion_dr
 | | |--output.tf
 | | |--main.tf
 | | |--dr_dc_network_config_gen.yaml
 | | |--variable.tf
 | | |--slave_director.sh


**main.tf file:**

main.tf template file will perform below actions/activities when executed:

- It will deploy DC components (Master_Director,Analytics,Search,DC service VNF, controller-1) in one region and deploy DR components (Slave_Director,DR service VNF,controller-2) in another region along with the IP details provide in tfvars.
- once instances deployed, Master_Director and Slave_Director will initialize the vnms-startup script in non-interactive mode
- DC and DR service VNF will initialize the cloud-init script along with BGP and IPSEC configuration,
- Master_Director and Slave_Director will form the HA,
- once HA is ready, then van_cluster_installer.py will start to integrate the analytics.
- Post van_cluster_installer.py DC service VNF, DR service VNF, controller-1 and Controller-2 will added to the Director.


**var.tf file:**

var.tf file will have definition for all the variables defined/used as part of this template. User does not need to update anything in this file.

**terraform.tfvars file:**

terraform.tfvars file is being used to get the variables values from user. User need to update the required fields according to their environment. This file will have below information:

###### provider INFO ######
dc_region : Provide the DC region deatils to deploy DC components. Eg "ap-southeast-1".
dr_region : Provide the DR region deatils to deploy DR components. Eg "us-west-1".
access_key : Provide the access key information here. This information will be obtained as part of terraform login.
secret_key : Provide the Secret key information here. This information will be obtained as part of terraform login.
tag_name : Provide the tag info , it will add the tag in every resource Eg: "customer-1"

###### file path example /Users/dinz/Downloads ######
key_pair_file_path : Provide the file path for the key pair Eg: "/Users/dinz/Downloads"
key_pair_name : Provide the key pair name

###### Host Name ######

hostname_dir_1 : Provide the hostname for master Director instances
hostname_dir_2 : Provide the hostname for slave Director instances
analytics_1_hostname : Provide the hostname for Analytics instances
search_1_hostname : Provide the hostname for search instances
controller_1_hostname : Provide the hostname for controller-1 instances
controller_1_country : Provide the country info for controller-1 instances
controller_2_hostname : Provide the hostname for controller-2 instances
controller_2_country : Provide the country info for controller-2 instances

###### DC and DR INSTANCE TYPE ######

Director_instance_type : Provide the instance type which will be used to provision the Versa Director Instance. By default, c5.2xlarge will be used.
analytics_instance_type : Provide the instance type which will be used to provision the Versa Analytics Instance. By default, c5.4xlarge will be used.
controller_instance_type : Provide the instance type which will be used to provision the Versa Controller Instance. By default, c5.2xlarge will be used.
router_instance_type : Provide the instance type which will be used to provision the Versa service VNF Instance. By default, c5.xlarge will be used.

###### AMI ######
DC_Director_ami : Provide the Versa Director ami for DC region
DC_analytics_ami : Provide the Versa Analytics ami for DC region
DC_controller_ami : Provide the Versa VOS ami for DC region
DR_Director_ami : Provide the Versa Director ami for DR region
DR_controller_ami : Provide the Versa VOS ami for DR region

###### DC-DR CIDR Subnet INFO ######

DC_cidr_block : Provide the DC VPC subnet info. By default "10.153.0.0/16" will be created in DC.
DR_cidr_block : Provide the DR VPC subnet info. By default "10.193.0.0/16" will be created in DR.

###### DC-DR CIDR MGNT Subnet INFO ######
###### DC MGNT Subnet INFO ######

DC_mgmt_subnet : Provide the DC management/North_bound subnet info. By default "10.153.0.0/24" will be created as management/North_bound subnet in DC.
DC_mgmt_subnet_gateway : Provide the DC management/North_bound gateway IP info. By default "10.153.0.1" will be created as management/North_bound gateway in DC.
DC_dir_ana_mgnt_interfaces_IP : Provide the management IP details for ["Master_Director","Analytics","Search"]. By default "10.153.0.21","10.153.0.25","10.153.0.26" will be created respectively.
DC_controller_flex_mgnt_interfaces_IP : Provide the management IP details for [DC_router,Controller-1,DC_DR_MGNT]. By default "10.153.0.22","10.153.0.23","10.153.0.24" will be created respectively.

#### DR CIDR MGNT Subnet INFO ####

DR_mgmt_subnet : Provide the DR management/North_bound subnet info. By default "10.193.0.0/24" will be created as management/North_bound subnet in DR.
DR_mgmt_subnet_gateway : Provide the DR management/North_bound gateway IP info. By default "10.193.0.1" will be created as management/North_bound gateway in DR.
DR_dir_ana_mgnt_interfaces_IP : Provide the management/North_bound IP details for ["Slave_Director"]. By default "10.193.0.21" will be created as slave Director management/North_bound.
DR_controller_flex_mgnt_interfaces_IP : Provide the management IP details for [DR_router,Controller2,DR_DC_MGNT]. By default "10.193.0.22","10.193.0.23","10.193.0.24" will be created respectively.

#### DC-DR CIDR INTERNET Subnet INFO ####
#### DC INTERNET Subnet INFO ####

DC_internet_subnet : Provide the DC internet subnet info. By default "10.153.1.0/24" will be created as internet subnet in DC.
DC_internet_subnet_gateway : Provide the DC internet gateway IP info. By default "10.153.1.1" will be created as internet gateway in DC.
DC_controller_flex_internet_network_interfaces_IP : Provide the DC internet IP details for [DC_router,Controller-1]. By default "10.153.1.22","10.153.1.23" will be created respectively.

#### DR CIDR INTERNET Subnet INFO ####

DR_internet_subnet : Provide the DR internet subnet info. By default "10.193.1.0/24" will be created as internet subnet in DR.
DR_internet_subnet_gateway : Provide the DR internet gateway IP info. By default "10.193.1.1" will be created as internet gateway in DR.
DR_controller_flex_internet_network_interfaces_IP Provide the internet IP details for [DR_router,Controller2]. By default "10.193.1.22","10.193.1.23" will be created respectively.

#### DC-DR CIDR SOUTH_BOUND Subnet INFO ####
#### DC CIDR SOUTH_BOUND Subnet INFO ####

DC_south_bound_network_subnet : Provide the DC south bound subnet info. By default "10.153.2.0/24" will be created as south bound subnet in DC.
DC_south_bound_network_interfaces_IP : Provide the south bound IP details for ["Master_Director","Analytics","Search",DC_router,Controller1]. By default "10.153.2.21","10.153.2.25","10.153.2.26","10.153.2.22","10.153.2.23" will be created respectively.

#### DR CIDR SOUTH_BOUND Subnet INFO ####

DR_south_bound_network_subnet : Provide the DR south bound subnet info. By default "10.193.2.0/24" will be created as south bound subnet in DR.
DR_south_bound_network_interfaces_IP : Provide the DR south bound IP details for [Slave_Director,DR_router,Controller2]. By default "10.193.2.21","10.193.2.22","10.193.2.23" will be created respectively.

#### Accessable subnet ####
If you want you can restrict the subnets
Public_subnet_resource_access ="0.0.0.0/0"

#### ORG NAME ####
parent_org_name = Provide the parent ORG name. By default "versa" will be used as parent ORG
overlay_prefixes = Provide the overlay prefixes . By default "10.0.0.0/8" will be used as overlay prefixes


**output.tf file:**

output.tf file will have information to provide the output
