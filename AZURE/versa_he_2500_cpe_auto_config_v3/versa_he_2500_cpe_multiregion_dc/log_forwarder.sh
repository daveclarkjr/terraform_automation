#!/bin/bash
log_path="/etc/bootLog.txt"
if [ -f "$log_path" ]
then
    echo "Cloud Init script already ran earlier during first time boot.." >> $log_path
else
    touch $log_path
SSHKey="${sshkey}"
KeyDir="/home/versa/.ssh"
KeyFile="/home/versa/.ssh/authorized_keys"
UBUNTU_RELEASE="$(lsb_release -cs)"
SSH_Conf="/etc/ssh/sshd_config"
DirIP_1="${master_dir_mgmt_ip}"
DirIP_2="${slave_dir_mgmt_ip}"
Address="Match Address $DirIP_1,$DirIP_2"
echo "Starting cloud init script..." > $log_path

echo "Modifying /etc/network/interface file.." >> $log_path
cp /etc/network/interfaces /etc/network/interfaces.bak
if [[ $UBUNTU_RELEASE == "trusty" ]]; then
cat > /etc/network/interfaces << EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet dhcp
post-up route add -net 0.0.0.0/0 gw ${mgmt_subnet_gateway} dev eth0
# The secondary network interface
auto eth1
iface eth1 inet dhcp
post-up route add -net ${DR_mgmt_subnet} gw ${dc_router_south_bound_ip} dev eth1
post-up route add -net ${DR_south_bound_network_subnet} gw ${dc_router_south_bound_ip} dev eth1
post-up route add -net ${DR_control_network_subnet} gw ${dc_router_south_bound_ip} dev eth1
# The third network interface
auto eth2
iface eth2 inet dhcp
EOF
else
cat > /etc/network/interfaces << EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback
# The primary network interface
auto eth0
iface eth0 inet dhcp
    offload-gro off
post-up route add -net 0.0.0.0/0 gw ${mgmt_subnet_gateway} dev eth0    
# The secondary network interface
auto eth1
iface eth1 inet dhcp
    offload-gro off    
post-up route add -net ${DR_mgmt_subnet} gw ${dc_router_south_bound_ip} dev eth1
post-up route add -net ${DR_south_bound_network_subnet} gw ${dc_router_south_bound_ip} dev eth1
post-up route add -net ${DR_control_network_subnet} gw ${dc_router_south_bound_ip} dev eth1
# The third network interface
auto eth2
iface eth2 inet dhcp
    offload-gro off
EOF
fi
echo -e "Modified /etc/network/interface file. Refer below new interface file content:\n`cat /etc/network/interfaces`" >> $log_path

echo "Restart Network services.." >> $log_path
if [[ $UBUNTU_RELEASE == "trusty" ]]; then
    /etc/init.d/networking restart >> /dev/null 2>&1
else
    systemctl restart networking >> /dev/null 2>&1
fi

echo -e "Injecting ssh key into versa user.\n" >> $log_path
if [ ! -d "$KeyDir" ]; then
    echo -e "Creating the .ssh directory and injecting the SSH Key.\n" >> $log_path
    sudo mkdir $KeyDir
    sudo echo $SSHKey >> $KeyFile
    sudo chown versa:versa $KeyDir
    sudo chown versa:versa $KeyFile
    sudo chmod 600 $KeyFile
elif ! grep -Fq "$SSHKey" $KeyFile; then
    echo -e "Key not found. Injecting the SSH Key.\n" >> $log_path
    sudo echo $SSHKey >> $KeyFile
    sudo chown versa:versa $KeyDir
    sudo chown versa:versa $KeyFile
    sudo chmod 600 $KeyFile
else
    echo -e "SSH Key already present in file: $KeyFile.." >> $log_path
fi

echo -e "Enanbling ssh login using password." >> $log_path
if ! grep -Fq "$Address" $SSH_Conf; then
    echo -e "Adding the match address exception for Director Management IP.\n" >> $log_path
    sed -i.bak "\$a\Match Address $DirIP_1,$DirIP_2\n  PasswordAuthentication yes\nMatch all" $SSH_Conf
    sudo service ssh restart
else
    echo -e "Director Management IP address is alredy present in file $SSH_Conf.\n" >> $log_path
fi
volume_extend_log_path="/tmp/volume_extend_Log.txt"
hd=$(lsblk | grep 120G | awk  '{print $1}')
if [ -f "\$volume_extend_log_path" ]
then
    echo "volume extension ran earlier.." >> $volume_extend_log_path
else
    (echo n; echo p; echo 3; echo 167770112; echo 251658239; echo t; echo 3; echo 8e; echo w) | fdisk /dev/$hd >> $volume_extend_log_path
    partprobe
    pvcreate /dev/$hd'3' >> $volume_extend_log_path
    vgextend system /dev/$hd'3' >> $volume_extend_log_path
    lvextend -l+100%FREE /dev/system/root >> $volume_extend_log_path
    resize2fs /dev/mapper/system-root >> $volume_extend_log_path    
fi
fi
