variable "tag_name" {
  description = "tag Name"
  # default = "dkumar_tf_"
}
variable "private_key_name"{
  description = "For output display"
}
variable "subscription_id" {
  description = "Subscription ID of Azure account."
}

variable "client_id" {
  description = "Client ID of Terraform account to be used to deploy VM on Azure."
}

variable "client_secret" {
  description = "Client Secret of Terraform account to be used to deploy VM on Azure."
}

variable "tenant_id" {
  description = "Tenant ID of Terraform account to be used to deploy VM on Azure."
}

variable "location" {
  description = "Location where Versa Head End setup to be deployed."
}

variable "availability_zone" {
  description = "availability_zone of Versa Head End setup to be deployed."
}

variable "resource_group" {
  description = "Name of the resource group in which Versa Head End setup will be deployed."
  default     = "Versa_HE"
}

variable "ssh_key" {
  description = "SSH Key to be injected into VMs deployed on Azure."
}

variable "vpc_address_space" {
  description = "Virtual Private Network's address space to be used to deploy Versa Head end setup."
  default     = "10.234.0.0/16"
}

variable "newbits_subnet" {
  description = "This is required to create desired netmask from virtual network."
  default     = "8"
}

variable "image_director" {
  description = "Versa Director Image ID to be used to deploy Versa Director."
}

variable "image_controller" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "image_svnf" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "hostname_director" {
  description = "Hostname to be used for Versa Director."
  type        = list(string)
  # default = [
  #   "versa-director-2",
  # ]
}
variable "hostname_dir_1" {
  description = "Director-1 host name"
  type        = list(string)
  # default = "versa-director-2"
}
variable "director_vm_size" {
  description = "Size of Versa Director VM."
  default     = "Standard_DS3"
}

variable "controller_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "svnf_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "dir_disk_size" {
  description = "Disk size to be provisioned for Director VMs."
  default     = "200"
}
variable "controller_disk_size" {
  description = "Disk size to be provisioned for controller VMs."
  default     = "120"
}

#Subnet_Info
variable "mgmt_subnet" {
  description = "Management Subnet for VM in AWS"
  # default = "10.193.0.0/24"
}
variable "south_bound_network_subnet" {
  description = "control network subnet for VM in AWS"
  # default = "10.193.1.0/24"
}
variable "control_network_subnet" {
  description = "control network subnet for VM in AWS"
  # default = "10.193.4.0/24"
}
variable "inter_network_subnet" {
  description = "control network subnet for VM in AWS"
  # default = "10.193.3.0/24"
}
variable "internet_subnet" {
  description = "Internet Subnet for VM in AWS"
  # default = "10.193.4.0/24"
}
variable "master_dir_mgmt_ip" {
 description = "Master Director managment IP for slave dirctor to start the startup script" 
}
variable "master_dir_south_bound_ip" {
 description = "Master Director south bound IP for controller and router access" 
}
variable "DC_mgmt_subnet" {
  description = "To add the route in DR"
}
variable "DC_south_bound_network_subnet" {
  description = "To add the route in DR"
}
variable "DC_internet_public_IP" {
  description = "Internet IP for IPsec tunnel"
}
variable "DC_internet_private_IP" {
  description = "Internet private IP for IPsec tunnel"
}

variable "parent_org_name" {
  description = "Define org to auto_deployment"
#   default = "versa"
}
variable "overlay_prefixes" {
  description = "overlay prefixes"
  # default = "10.0.0.0/8"
}
variable "mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.193.0.1"
}
variable "DR_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.193.3.1"
}
variable "dr_south_bound_network_gateway" {
  description = "South Bound Network Gateway for north Bound rechability"
  # default = "10.193.1.1"
}
variable "inter_network_subnet_gateway" {
  description = "Inter Network Gateway for rechability"
  # default = "10.193.2.1"
}
variable "Versa_Analytics_1_Instance_south_bound_IP" {
  description = "Versa_Analytics_1_Instance_south_bound_IP"
}
variable "Versa_Analytics_2_Instance_south_bound_IP" {
  description = "Versa_Analytics_2_Instance_south_bound_IP"
}
variable "Versa_Analytics_3_Instance_south_bound_IP" {
  description = "Versa_Analytics_3_Instance_south_bound_IP"
}
variable "Versa_Analytics_4_Instance_south_bound_IP" {
  description = "Versa_Analytics_4_Instance_south_bound_IP"
}
variable "Versa_Search_1_Instance_south_bound_IP" {
  description = "Versa_Search_1_Instance_south_bound_IP"
}
variable "Versa_Search_2_Instance_south_bound_IP" {
  description = "Versa_Search_2_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_1_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_1_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_2_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_2_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_3_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_3_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_4_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_4_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_5_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_5_Instance_south_bound_IP"
}
variable "Versa_Log_forwarder_6_Instance_south_bound_IP" {
  description = "Versa_Log_forwarder_6_Instance_south_bound_IP"
}

#Interface IP details

variable "DR_dir_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Slave_Director"
# default = ["10.193.0.21"]
}
variable "DR_controller_flex_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for DR_router,Controller2"
# default = ["10.193.0.22","10.193.0.23"]
}
variable "DR_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Slave_Director,DR_router"
# default = ["10.193.1.21","10.193.1.22"]
}
variable "DR_control_network_interface_IP" {
description = "Control network Interface Ip for DR_router,Controller2"
# default = ["10.193.4.22","10.193.4.23"]
}
variable "DR_inter_network_interface_IP" {
description = "Inter network Interface Ip for DR_router"
# default = ["10.193.2.22"]
}
variable "DR_internet_subnet_interface_IP" {
description = "Internet Interface Ip for Controller2"
# default = ["10.193.3.23","10.193.3.24"]
}