variable "tag_name" {
  description = "tag Name"
  # default = "dkumar_tf_"
}
variable "private_key_name"{
  description = "For output display"
}
variable "subscription_id" {
  description = "Subscription ID of Azure account."
}

variable "client_id" {
  description = "Client ID of Terraform account to be used to deploy VM on Azure."
}

variable "client_secret" {
  description = "Client Secret of Terraform account to be used to deploy VM on Azure."
}

variable "tenant_id" {
  description = "Tenant ID of Terraform account to be used to deploy VM on Azure."
}

variable "location" {
  description = "Location where Versa Head End setup to be deployed."
}
variable "availability_zone" {
  description = "availability_zone of Versa Head End setup to be deployed."
}

variable "resource_group" {
  description = "Name of the resource group in which Versa Head End setup will be deployed."
  default     = "Versa_HE"
}

variable "ssh_key" {
  description = "SSH Key to be injected into VMs deployed on Azure."
}

variable "vpc_address_space" {
  description = "Virtual Private Network's address space to be used to deploy Versa Head end setup."
  default     = "10.234.0.0/16"
}

variable "newbits_subnet" {
  description = "This is required to create desired netmask from virtual network."
  default     = "8"
}

variable "image_director" {
  description = "Versa Director Image ID to be used to deploy Versa Director."
}

variable "image_controller" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "image_svnf" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "image_analytics" {
  description = "Versa Analytics Image ID to be used to deploy Versa Analytics."
}

variable "hostname_director" {
  description = "Hostname to be used for Versa Director."
  type        = list(string)
  default = [
    "versa-director-1",
  ]
}

variable "Slave_director_Hostname" {
  description = "Director-2 host name"
  type        = list(string)
  # default = "versa-director-2"
}

variable "Master_director_Hostname" {
  description = "Director-1 host name"
  type        = list(string)
  # default = "versa-director-2"
}
variable "hostname_van" {
  description = "Hostname to be used for Versa Analytics nodes. Number of analytics instances created here will be equal to number of hostname entries made. All VAN instances will be created in primary region only."
  type        = list(string)
  default = [
    "versa-analytics-1",
    "versa-analytics-2",
  ]
}

variable "director_vm_size" {
  description = "Size of Versa Director VM."
  default     = "Standard_DS3"
}

variable "controller_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "svnf_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "analytics_vm_size" {
  description = "Size of Versa Analytics VM."
  default     = "Standard_DS3"
}

variable "van_disk_size" {
  description = "Disk size to be provisioned for Analytics VMs."
  default     = "80"
}


variable "parent_org_name" {
  description = "Define org to auto_deployment"
#   default = "versa"
}


variable "overlay_prefixes" {
  description = "overlay prefixes"
  # default = "10.0.0.0/8"
}

variable "controller_1_hostname" {
  description = "controller 1 host name"
  # default = "Controller-1"  
}
variable "controller_1_country" {
  description = "controller 1 country name"
  # default = "Singapore"  
}
variable "controller_2_hostname" {
  description = "controller 2 host name"
  # default = "Controller-2"  
}
variable "controller_2_country" {
  description = "controller 2 country name"
  # default = "california"  
}
#Subnet_Info
variable "mgmt_subnet" {
  description = "Management Subnet for VM in AWS"
  # default = "10.153.0.0/24"
}
variable "mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.153.0.1"
}

variable "south_bound_network_subnet" {
  description = "South Bound network subnet for VM in AWS"
  # default = "10.153.1.0/24"
}

variable "ctrl_1_network_subnet" {
  description = "Controller_1 to svnf"
  # default = "10.153.4.0/24"
}
variable "ctrl_2_network_subnet" {
  description = "Controller_2 to svnf"
  # default = "10.153.4.0/24"
}
variable "internet_subnet" {
  description = "Internet Subnet for VM in AWS"
  # default = "10.153.4.0/24"
}
variable "internet_subnet_gateway" {
  description = "Internet Subnet gateway for VM in AWS"
  # default = "10.153.4.0/24"
}

variable "dir_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Master_Directo and slave_director"
# default = ["10.153.0.21","10.153.0.22"]
}
variable "ana_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Analytics,Search"
# default = ["10.153.0.26","10.153.0.27"]
}
variable "controller_flex_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for service_vnf,Controller1,Controller2"
# default = ["10.153.0.23","10.153.0.24","10.153.0.25"]
}
variable "dir_south_bound_interfaces_IP" {
description = "south_bound Interface Ip for Master_Directo and slave_director"
# default = ["10.153.1.21","10.153.1.22"]
}
variable "ana_south_bound_interfaces_IP" {
description = "south_bound Interface Ip for Analytics,Search"
# default = ["10.153.1.26","10.153.1.27"]
}
variable "service_vnf_south_bound_interfaces_IP" {
description = "south_bound Interface Ip for service_vnf"
# default = ["10.153.1.23"]
}
variable "service_vnf_control_network_1_interfaces_IP" {
description = "control_network_1 Interface Ip for service_vnf"
# default = ["10.153.2.23"]
}
variable "controller_1_control_network_interfaces_IP" {
description = "control_network_1 Interface Ip for Controller1"
# default = ["10.153.2.24"]
}
variable "service_vnf_control_network_2_interfaces_IP" {
description = "control_network_2 Interface Ip for service_vnf"
# default = ["10.153.3.23"]
}
variable "controller_2_control_network_interfaces_IP" {
description = "control_network_2 Interface Ip for Controller2"
# default = ["10.153.3.24"]
}
variable "controller_internet_interfaces_IP" {
description = "Internet Interface Ip for Controller1,Controller2"
# default = ["10.153.4.24","10.153.4.25"]
}