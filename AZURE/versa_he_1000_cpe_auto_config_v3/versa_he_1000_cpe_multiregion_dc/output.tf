#CLI_LOGIN Details

output "Master_Director_CLI_sshCommand" {
  value = "ssh -i ${var.private_key_name}.pem Administrator@${azurerm_public_ip.ip_dir[0].ip_address}"
}
# output "DC_Router_CLI_sshCommand" {
#   value = "ssh -i ${var.private_key_name}.pem admin@${azurerm_public_ip.ip_svnf[0].ip_address}"
# }
# output "Versa_Controller-1_CLI_sshCommand" {
#   value = "ssh -i ${var.private_key_name}.pem admin@${azurerm_public_ip.ip_controller[0].ip_address}"
# }

#UI LOGIN DETAILS
output "Master_Director_UI_Login" {
  value = "https://${azurerm_public_ip.ip_dir[0].ip_address}"
}

#Instance Name

output "Master_Director_Instance" {
  value = azurerm_virtual_machine.directorVM[0].name
}
output "Versa_Analytics-1_Instance" {
  value = azurerm_virtual_machine.vanVM[0].name
}
output "Versa_Analytics-2_Instance" {
  value = azurerm_virtual_machine.vanVM[1].name
}
output "Versa_Search-1_Instance" {
  value = azurerm_virtual_machine.vanVM[2].name
}
output "Versa_Search-2_Instance" {
  value = azurerm_virtual_machine.vanVM[3].name
}
output "DC_Router_Instance" {
  value = azurerm_virtual_machine.svnfVM[0].name
}
output "Versa_Controller-1_Instance" {
  value = azurerm_virtual_machine.controllerVM[0].name
}

#Private_MGNT_IP
output "Master_Director_MGMT_IP" {
  value = azurerm_network_interface.director_nic_1[0].private_ip_address
}
output "Versa_Analytics_1_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[0].private_ip_address
}
output "Versa_Analytics_1_Instance_south_bound_IP" {
  value = azurerm_network_interface.van_nic_2[0].private_ip_address
}
output "Versa_Analytics_2_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[1].private_ip_address
}
output "Versa_Analytics_2_Instance_south_bound_IP" {
  value = azurerm_network_interface.van_nic_2[1].private_ip_address
}
output "Versa_Search_1_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[2].private_ip_address
}
output "Versa_Search_1_Instance_south_bound_IP" {
  value = azurerm_network_interface.van_nic_2[2].private_ip_address
}
output "Versa_Search_2_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[3].private_ip_address
}
output "Versa_Search_2_Instance_south_bound_IP" {
  value = azurerm_network_interface.van_nic_2[3].private_ip_address
}
output "DC_Router_MGMT_IP" {
  value = azurerm_network_interface.svnf_nic_1.private_ip_address
}
output "Versa_Controller-1_MGMT_IP" {
  value = azurerm_network_interface.controller_nic_1.private_ip_address
}

#Public MGNT IP

output "Master_Director_Public_IP" {
  value = azurerm_public_ip.ip_dir[0].ip_address
}
# output "Versa_Analytics-1_Instance_Public_IP" {
#   value = azurerm_public_ip.ip_van[0].ip_address
# }
# output "Versa_Analytics-2_Instance_Public_IP" {
#   value = azurerm_public_ip.ip_van[1].ip_address
# }
# output "Versa_Controller-1_Public_IP" {
#   value = azurerm_public_ip.ip_controller[0].ip_address
# }
# output "DC_Router_Public_IP" {
#   value = azurerm_public_ip.ip_svnf[0].ip_address
# }

#South Bound IP
output "Master_Director_south_bound_IP" {
  value = azurerm_network_interface.director_nic_2[0].private_ip_address
}

# Internet Transport IP

output "Versa_Controller-1_InternetTransport_Public_IP" {
  value = azurerm_public_ip.ip_controller_inet[0].ip_address
}
output "DC_Router_Internet_private_IP" {
  value = azurerm_network_interface.svnf_nic_3.private_ip_address
}
output "DC_Router_Internet_Public_IP" {
  value = azurerm_public_ip.ip_svnf_inet[0].ip_address
}

#Script Execution
output "DC_mgmt_subnet" {
  value = azurerm_subnet.mgmt_subnet.address_prefix
}

