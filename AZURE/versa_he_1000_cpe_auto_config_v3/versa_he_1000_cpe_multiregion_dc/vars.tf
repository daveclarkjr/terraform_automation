variable "tag_name" {
  description = "tag Name"
  # default = "dkumar_tf_"
}
variable "private_key_name"{
  description = "For output display"
}
variable "subscription_id" {
  description = "Subscription ID of Azure account."
}

variable "client_id" {
  description = "Client ID of Terraform account to be used to deploy VM on Azure."
}

variable "client_secret" {
  description = "Client Secret of Terraform account to be used to deploy VM on Azure."
}

variable "tenant_id" {
  description = "Tenant ID of Terraform account to be used to deploy VM on Azure."
}

variable "location" {
  description = "Location where Versa Head End setup to be deployed."
}
variable "availability_zone" {
  description = "availability_zone of Versa Head End setup to be deployed."
}

variable "resource_group" {
  description = "Name of the resource group in which Versa Head End setup will be deployed."
  default     = "Versa_HE"
}

variable "ssh_key" {
  description = "SSH Key to be injected into VMs deployed on Azure."
}

variable "vpc_address_space" {
  description = "Virtual Private Network's address space to be used to deploy Versa Head end setup."
  default     = "10.234.0.0/16"
}

variable "newbits_subnet" {
  description = "This is required to create desired netmask from virtual network."
  default     = "8"
}

variable "image_director" {
  description = "Versa Director Image ID to be used to deploy Versa Director."
}

variable "image_controller" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "image_svnf" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "image_analytics" {
  description = "Versa Analytics Image ID to be used to deploy Versa Analytics."
}

variable "hostname_director" {
  description = "Hostname to be used for Versa Director."
  type        = list(string)
  default = [
    "versa-director-1",
  ]
}

variable "hostname_dir_2" {
  description = "Director-2 host name"
  type        = list(string)
  # default = "versa-director-2"
}

variable "hostname_dir_1" {
  description = "Director-1 host name"
  type        = list(string)
  # default = "versa-director-2"
}
variable "hostname_van" {
  description = "Hostname to be used for Versa Analytics nodes. Number of analytics instances created here will be equal to number of hostname entries made. All VAN instances will be created in primary region only."
  type        = list(string)
  default = [
    "versa-analytics-1",
    "versa-analytics-2",
  ]
}

variable "director_vm_size" {
  description = "Size of Versa Director VM."
  default     = "Standard_DS3"
}

variable "controller_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "svnf_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "analytics_vm_size" {
  description = "Size of Versa Analytics VM."
  default     = "Standard_DS3"
}

variable "dir_disk_size" {
  description = "Disk size to be provisioned for Director VMs."
  default     = "200"
}
variable "van_disk_size" {
  description = "Disk size to be provisioned for Analytics VMs."
  default     = "512"
}
variable "controller_disk_size" {
  description = "Disk size to be provisioned for controller VMs."
  default     = "120"
}


variable "parent_org_name" {
  description = "Define org to auto_deployment"
#   default = "versa"
}
variable "slave_dir_mgmt_ip" {
 description = "Slave Director managment IP for for controller and router access" 
}
variable "slave_dir_south_bound_ip" {
 description = "Slave Director south bound IP for controller and router access" 
}
variable "DR_mgmt_subnet"{
  description = "To add the route in DC"
}
variable "DR_internet_public_IP" {
  description = "Internet IP for IPsec tunnel"
}
variable "DR_internet_private_IP" {
  description = "Internet private IP for IPsec tunnel"
}
variable "DR_router_eth0_mgnt_ip" {
  description = "DR_router_eth0_mgnt_ip for auto device deployment"
}

variable "overlay_prefixes" {
  description = "overlay prefixes"
  # default = "10.0.0.0/8"
}

variable "controller_1_hostname" {
  description = "controller 1 host name"
  # default = "Controller-1"  
}
variable "controller_1_country" {
  description = "controller 1 country name"
  # default = "Singapore"  
}
variable "controller_2_hostname" {
  description = "controller 2 host name"
  # default = "Controller-2"  
}
variable "controller_2_country" {
  description = "controller 2 country name"
  # default = "california"  
}
#Subnet_Info
variable "mgmt_subnet" {
  description = "Management Subnet for VM in AWS"
  # default = "10.153.0.0/24"
}
variable "south_bound_network_subnet" {
  description = "south bound network subnet for VM in AWS"
  # default = "10.153.1.0/24"
}
variable "control_network_subnet" {
  description = "control network subnet for VM in AWS"
  # default = "10.153.4.0/24"
}
variable "DR_south_bound_network_subnet" {
  description = "DR_south_bound_network_subnet"
}
variable "DR_control_network_subnet" {
  description = "DR_control_network_subnet"
}
variable "inter_network_subnet" {
  description = "control network subnet for VM in AWS"
  # default = "10.153.3.0/24"
}
variable "internet_subnet" {
  description = "Internet Subnet for VM in AWS"
  # default = "10.153.4.0/24"
}
#Gateway_info
variable "mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.153.0.1"
}
variable "inter_network_subnet_gateway" {
  description = "Inter Network Gateway for rechability"
  # default = "10.153.2.1"
}
variable "DC_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.153.3.1"
}
variable "dc_south_bound_network_gateway" {
  description = "South Bound Network Gateway for north Bound rechability"
  # default = "10.153.1.1"
}

#controller_2 Configuration
variable "controller_2_mgnt_ip" {
  description = "Managment IP of controller 2"
}
variable "controller_2_south_bound_subent" {
  description = "controller 2 south bound subnet"
}
variable "controller_2_internet_subent" {
  description = "controller 2 internet subnet"
}
variable "DR_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.193.3.1"
}
variable "controller_2_south_bound_ip" {
  description = "South Bound IP of controller 2"
}
variable "controller_2_internet_private_ip" {
  description = "Private internet IP of controller 2"
}
variable "controller_2_internet_public_ip" {
  description = "Public internet IP of controller 2"
}
variable "dr_router_south_bound_ip" {
  description = "DR Router south bound IP"
}
variable "dr_router_control_ntw_ip" {
  description = "DR Router control network IP"
}


#Interface IP details

variable "DC_dir_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Master_Director"
# default = ["10.153.0.21"]
}
variable "DC_ana_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Analytics,Search"
# default = ["10.153.0.25","10.153.0.26","10.153.0.27","10.153.0.28"]
}
variable "DC_controller_flex_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for DC_router,Controller1"
# default = ["10.153.0.22","10.153.0.23"]
}
variable "DC_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Master_Director,DC_router"
# default = ["10.153.1.21","10.153.1.22"]
}
variable "DC_ana_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Analytics,Search"
# default = ["10.153.1.25","10.153.1.26","10.153.1.27","10.153.1.28"]
}
variable "DC_control_network_interface_IP" {
description = "Control network Interface Ip for DC_router,Controller1"
# default = ["10.153.4.22","10.153.4.23"]
}
variable "DC_inter_network_interface_IP" {
description = "Inter network Interface Ip for DC_router"
# default = ["10.153.2.22"]
}
variable "DC_internet_subnet_interface_IP" {
description = "Internet Interface Ip for Controller1"
# default = ["10.153.3.23","10.153.3.24"]
}
