#common variables

variable "tag_name" {
  description = "tag Name"
  # default = "dkumar_tf_"
}
variable "private_key_name"{
  description = "For output display"
}
variable "subscription_id" {
  description = "Subscription ID of Azure account."
}

variable "client_id" {
  description = "Client ID of Terraform account to be used to deploy VM on Azure."
}

variable "client_secret" {
  description = "Client Secret of Terraform account to be used to deploy VM on Azure."
}

variable "tenant_id" {
  description = "Tenant ID of Terraform account to be used to deploy VM on Azure."
}

variable "ssh_key" {
  description = "SSH Key to be injected into VMs deployed on Azure."
}


variable "hostname_van" {
  description = "Hostname to be used for Versa Analytics nodes. Number of analytics instances created here will be equal to number of hostname entries made. All VAN instances will be created in primary region only."
  type        = list(string)
  default = [
    "versa-analytics-1",
    "versa-analytics-2",
  ]
}

variable "director_vm_size" {
  description = "Size of Versa Director VM."
  default     = "Standard_DS3"
}

variable "controller_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "svnf_vm_size" {
  description = "Size of Versa Controller VM."
  default     = "Standard_DS3"
}

variable "analytics_vm_size" {
  description = "Size of Versa Analytics VM."
  default     = "Standard_DS3"
}

variable "dir_disk_size" {
  description = "Disk size to be provisioned for Director VMs."
  default     = "200"
}
variable "van_disk_size" {
  description = "Disk size to be provisioned for Analytics VMs."
  default     = "512"
}
variable "controller_disk_size" {
  description = "Disk size to be provisioned for controller VMs."
  default     = "120"
}

#DC variables

variable "dc_location" {
  description = "Location where Versa Head End setup to be deployed."
}

variable "dc_availability_zone" {
  description = "availability_zone of Versa Head End setup to be deployed."
}

variable "dc_resource_group" {
  description = "Name of the resource group in which Versa Head End setup will be deployed."
  default     = "Versa_HE_DC"
}

variable "dc_vpc_address_space" {
  description = "Virtual Private Network's address space to be used to deploy Versa Head end setup."
  default     = "10.234.0.0/16"
}

variable "dc_newbits_subnet" {
  description = "This is required to create desired netmask from virtual network."
  default     = "8"
}
variable "dc_hostname_director" {
  description = "Hostname to be used for Versa Director."
  type        = list(string)
  default = [
    "versa-director-1",
  ]
}
variable "dc_image_director" {
  description = "Versa Director Image ID to be used to deploy Versa Director."
}

variable "dc_image_controller" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "dc_image_svnf" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "dc_image_analytics" {
  description = "Versa Analytics Image ID to be used to deploy Versa Analytics."
}
#DR variables

variable "dr_location" {
  description = "Location where Versa Head End setup to be deployed."
}

variable "dr_availability_zone" {
  description = "availability_zone of Versa Head End setup to be deployed."
}
variable "dr_resource_group" {
  description = "Name of the resource group in which Versa Head End setup will be deployed."
  default     = "Versa_HE_DR"
}

variable "dr_vpc_address_space" {
  description = "Virtual Private Network's address space to be used to deploy Versa Head end setup."
  default     = "10.227.0.0/16"
}

variable "dr_newbits_subnet" {
  description = "This is required to create desired netmask from virtual network."
  default     = "8"
}
variable "dr_hostname_director" {
  description = "Hostname to be used for Versa Director."
  type        = list(string)
  default = [
    "versa-director-2",
  ]
}
variable "dr_image_director" {
  description = "Versa Director Image ID to be used to deploy Versa Director."
}

variable "dr_image_controller" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "dr_image_svnf" {
  description = "Controller/FlexVNF Image ID to be used to deploy Versa Controller."
}

variable "controller_1_hostname" {
  description = "controller 1 host name"
  # default = "Controller-1"  
}
variable "controller_1_country" {
  description = "controller 1 country name"
  # default = "Singapore"  
}
variable "controller_2_hostname" {
  description = "controller 2 host name"
  # default = "Controller-2"  
}
variable "controller_2_country" {
  description = "controller 2 country name"
  # default = "california"  
}
# #DC-DR CIDR Subnet INFO

# variable "DC_cidr_block" {
#     description = "IPV4 CIDR for VPC creation in DC"
#     # default = "10.153.0.0/16"
# }
# variable "DR_cidr_block" {
#     description = "IPV4 CIDR for VPC creation in DR"
#     # default = "10.193.0.0/16"
# }
# #DC-DR CIDR MGNT Subnet INFO
# #### DC CIDR MGNT Subnet INFO ####
variable "DC_mgmt_subnet" {
  description = "Management Subnet for VM in AWS DC"
  # default = "10.153.0.0/24"
}
variable "DC_mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.153.0.1"
}
# variable "DC_dir_ana_mgnt_interfaces_IP" {
#   # default = ["10.153.0.21","10.153.0.25","10.153.0.26"]
# }
# variable "DC_controller_flex_mgnt_interfaces_IP" {
#   # default = ["10.153.0.22","10.153.0.23","10.153.0.24"]
# }

# #### DC CIDR MGNT Subnet INFO ####
variable "DC_control_network_subnet" {
  description = "control network Subnet for VM in AWS DC"
  # default = "10.153.4.0/24"
}

# #### DR CIDR MGNT Subnet INFO ####
variable "DR_control_network_subnet" {
  description = "control network Subnet for VM in AWS DR"
  # default = "10.193.4.0/24"
}

# #### DR CIDR MGNT Subnet INFO ####
variable "DR_mgmt_subnet" {
  description = "Management Subnet for VM in AWS DR"
  # default = "10.193.0.0/24"
}
variable "DR_mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.193.0.1"
}
# variable "DR_dir_ana_mgnt_interfaces_IP" {
#   # default = ["10.193.0.21"]
# } 
# variable "DR_controller_flex_mgnt_interfaces_IP" {
#   # default = ["10.193.0.22","10.193.0.23","10.193.0.24"]
# }
# #DC-DR CIDR SOUTH_BOUND Subnet INFO
# #### DC CIDR SOUTH_BOUND Subnet INFO ####
variable "DC_south_bound_network_subnet" {
  description = "control network subnet for VM in AWS DC"
  # default = "10.153.1.0/24"
}
variable "dc_south_bound_network_gateway" {
  description = "South Bound Network Gateway for north Bound rechability"
  # default = "10.153.1.1"
}
# variable "DC_south_bound_network_interfaces_IP" {
#   # default = ["10.153.1.21","10.153.1.25","10.153.1.26","10.153.1.22","10.153.1.23"]
# }
# #### DR CIDR SOUTH_BOUND Subnet INFO ####
variable "DR_south_bound_network_subnet" {
  description = "control network subnet for VM in AWS DR"
  # default = "10.193.1.0/24"
}
variable "dr_south_bound_network_gateway" {
  description = "South Bound Network Gateway for north Bound rechability"
  # default = "10.153.1.1"
}
# variable "DR_south_bound_network_interfaces_IP" {
#   # default = ["10.193.1.21","10.193.1.22","10.193.1.23"]
# } 
# #DC-DR CIDR INTER NETWORK Subnet INFO
# #### DC CIDR INTER NETWORK Subnet INFO ####
variable "DC_inter_network_subnet" {
  description = "Internet Subnet for VM in AWS DC"
  # default = "10.153.2.0/24"
}
variable "DC_inter_network_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.153.2.1"
}
# #### DR CIDR INTER NETWORK Subnet INFO ####
variable "DR_inter_network_subnet" {
  description = "Internet Subnet for VM in AWS DC"
  # default = "10.193.2.0/24"
}
variable "DR_inter_network_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.193.2.1"
}

# variable "DC_controller_flex_internet_network_interfaces_IP" {
#   # default = ["10.153.1.22","10.153.1.23"]
# }
# #### DC CIDR INTERNET Subnet INFO ####
variable "DC_internet_subnet" {
  description = "Internet Subnet for VM in AWS DR"
  # default = "10.153.3.0/24"
}
variable "DC_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.153.3.1"
}
# #### DR CIDR INTERNET Subnet INFO ####
variable "DR_internet_subnet" {
  description = "Internet Subnet for VM in AWS DR"
  # default = "10.193.3.0/24"
}
variable "DR_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.193.3.1"
}
# variable "DR_controller_flex_internet_network_interfaces_IP" {
#   # default = ["10.193.1.22","10.193.1.23"]
# }

# #DC-DR PUBLIC ACCESS Subnet INFO
# variable "Public_subnet_resource_access" {
#   description = "Define public IP to access the resources"
# #   default = "103.77.37.189/32"
# }
variable "parent_org_name" {
  description = "Define org to auto_deployment"
#   default = "versa"
}
variable "overlay_prefixes" {
  description = "overlay prefixes"
  # default = "10.0.0.0/8"
}
#Interface IP details

variable "DC_dir_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Master_Director"
# default = ["10.153.0.21"]
}
variable "DC_ana_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Analytics,Search"
# default = ["10.153.0.25","10.153.0.26","10.153.0.27","10.153.0.28"]
}
variable "DC_controller_flex_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for DC_router,Controller1"
# default = ["10.153.0.22","10.153.0.23"]
}
variable "DC_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Master_Director,DC_router"
# default = ["10.153.1.21","10.153.1.22"]
}
variable "DC_ana_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Analytics,Search"
# default = ["10.153.1.25","10.153.1.26","10.153.1.27","10.153.1.28"]
}
variable "DC_control_network_interface_IP" {
description = "Control network Interface Ip for DC_router,Controller1"
# default = ["10.153.4.22","10.153.4.23"]
}
variable "DC_inter_network_interface_IP" {
description = "Inter network Interface Ip for DC_router"
# default = ["10.153.2.22"]
}
variable "DC_internet_subnet_interface_IP" {
description = "Internet Interface Ip for Controller1"
# default = ["10.153.3.23","10.153.3.24"]
}

variable "DR_dir_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for Slave_Director"
# default = ["10.193.0.21"]
}
variable "DR_controller_flex_mgnt_interfaces_IP" {
description = "Mgnt Interface Ip for DR_router,Controller2"
# default = ["10.193.0.22","10.193.0.23"]
}
variable "DR_south_bound_network_interfaces_IP" {
description = "South Bound Interface Ip for Slave_Director,DR_router"
# default = ["10.193.1.21","10.193.1.22"]
}
variable "DR_control_network_interface_IP" {
description = "Control network Interface Ip for DR_router,Controller2"
# default = ["10.193.4.22","10.193.4.23"]
}
variable "DR_inter_network_interface_IP" {
description = "Inter network Interface Ip for DR_router"
# default = ["10.193.2.22"]
}
variable "DR_internet_subnet_interface_IP" {
description = "Internet Interface Ip for Controller2"
# default = ["10.193.3.23","10.193.3.24"]
}
