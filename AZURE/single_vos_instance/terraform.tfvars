#----------------------------------------------------------------------
# Variable's value defined here
#----------------------------------------------------------------------

#common variables

#common variables
subscription_id =  "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_secret = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tenant_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tag_name = "customer_name_tf"
private_key_name = "customer_name"
ssh_key =  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bi1jnbgPOVHq2d1Vu1lpc+i3HfRlCxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

location		= "Central India"
resource_group		= "dkumar_FlexVNF_RG_TF"
image_flexvnf		= "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/lab_dkumar_tf_resource_group/providers/Microsoft.Compute/images/lab_dkumar_tf_controller_21.3.1_B"
flexvnf_vm_size		= "Standard_F4s"

#Subnet_Info
virtual_network_subnet = "10.0.0.0/16"
mgnt_subnet = "10.0.1.0/24"
wan_subnet = "10.0.2.0/24"
lan_subnet = "10.0.3.0/24"

#Staging INFO
master_dir_south_bound_ip = "172.23.1.2"
slave_dir_south_bound_ip = "2.2.2.2"
controller_ip =  "103.231.208.60"
wan_gateway = "10.0.2.1"
local_identifier = "SDWAN-Branch@Versa.com"
remote_identifier = "Controller01-staging@Versa.com"
serial_number = "Azure_tf_single_vos_dinz"