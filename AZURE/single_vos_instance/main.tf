# Configure the Microsoft Azure Provider
provider "azurerm" {
    subscription_id = var.subscription_id
    client_id       = var.client_id
    client_secret   = var.client_secret
    tenant_id       = var.tenant_id
    # version         = "~>2.6.0"
    features {}
}

# provider "random" {
#     version = "~> 2.2"
# }

# provider "template" {
#     version = "~> 2.1"
# }

# Create a resource group
resource "azurerm_resource_group" "versa_rg" {
    name     = var.resource_group
    location = var.location

    tags = {
        environment = "VersaFlexVNF"
    }
}

# Create Management Public IP for FlexVNF
resource "azurerm_public_ip" "ip_flexvnf_mgmt" {
    name						= "PublicIP_FlexVNF_mgnt"
    location                    = var.location
    resource_group_name         = azurerm_resource_group.versa_rg.name
    # sku                 = "Standard"
    # allocation_method   = "Static"
    allocation_method 			= "Dynamic"
    tags = {
        environment = "VersaFlexVNF"
    }
}
# Create Management Public IP for FlexVNF
resource "azurerm_public_ip" "ip_flexvnf_wan" {
    name						= "PublicIP_FlexVNF_wan"
    location                    = var.location
    resource_group_name         = azurerm_resource_group.versa_rg.name
    # sku                 = "Standard"
    # allocation_method   = "Static"
    allocation_method 			= "Dynamic"
    tags = {
        environment = "VersaFlexVNF"
    }
}

# Create Network Security Group and rule for Mgnt Network
resource "azurerm_network_security_group" "versa_nsg_mgnt_network" {
    name                = "VersaNSG_Mgnt_Network"
    location            = var.location
    resource_group_name = azurerm_resource_group.versa_rg.name
	security_rule  {
        name                       = "Versa_Security_Rule_UDP"
        priority                   = 201
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_ranges     = ["22"]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
	security_rule  {
        name                       = "Versa_Security_Rule_Outbound"
        priority                   = 251
        direction                  = "Outbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    tags = {
        environment = "VersaHeadEnd"
    }
}
# Create Network Security Group and rule for wan Network
resource "azurerm_network_security_group" "versa_nsg_wan_network" {
    name                = "VersaNSG_Wan_Network"
    location            = var.location
    resource_group_name = azurerm_resource_group.versa_rg.name
	security_rule  {
        name                       = "Versa_Security_Rule_UDP"
        priority                   = 201
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Udp"
        source_port_range          = "*"
        destination_port_ranges     = ["500", "4500", "4790"]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
	security_rule  {
        name                       = "Versa_Security_Rule_Outbound"
        priority                   = 251
        direction                  = "Outbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    tags = {
        environment = "VersaHeadEnd"
    }
}
# Create Network Security Group and rule for LAN Network
resource "azurerm_network_security_group" "versa_nsg_lan_network" {
    name                = "VersaNSG_Lan_Network"
    location            = var.location
    resource_group_name = azurerm_resource_group.versa_rg.name
	security_rule  {
        name                       = "Versa_Security_Rule_Inbound"
        priority                   = 201
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "VirtualNetwork"
        destination_address_prefix = "VirtualNetwork"
    }
	security_rule  {
        name                       = "Versa_Security_Rule_Outbound"
        priority                   = 251
        direction                  = "Outbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "VirtualNetwork"
        destination_address_prefix = "VirtualNetwork"
    }
    tags = {
        environment = "VersaHeadEnd"
    }
}

resource "azurerm_virtual_network" "versa_tf_virtual_network" {
  name                = "${var.tag_name}_virtual_network"
  address_space       = [var.virtual_network_subnet]
  location            = var.location
  resource_group_name       = azurerm_resource_group.versa_rg.name
}

resource "azurerm_subnet" "versa_tf_mgnt_subnet" {
  name                 = "${var.tag_name}_mgnt_subnet"
  resource_group_name       = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versa_tf_virtual_network.name
  address_prefixes     = [var.mgnt_subnet]
}
resource "azurerm_subnet" "versa_tf_wan_subnet" {
  name                 = "${var.tag_name}_wan_subnet"
  resource_group_name       = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versa_tf_virtual_network.name
  address_prefixes     = [var.wan_subnet]
}
resource "azurerm_subnet" "versa_tf_lan_subnet" {
  name                 = "${var.tag_name}_lan_subnet"
  resource_group_name       = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versa_tf_virtual_network.name
  address_prefixes     = [var.lan_subnet]
}

# Create Management network interface for FlexVNF
resource "azurerm_network_interface" "flexvnf_nic_1" {
    name                      = "FlexVNF_NIC1"
    location                  = var.location
    resource_group_name       = azurerm_resource_group.versa_rg.name
	
    ip_configuration {
        name                          = "FlexVNF_NIC1_Configuration"
        subnet_id                     = azurerm_subnet.versa_tf_mgnt_subnet.id
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = azurerm_public_ip.ip_flexvnf_mgmt.id
    }
    tags = {
        environment = "VersaFlexVNF"
    }
}

# Create WAN network interface for FlexVNF
resource "azurerm_network_interface" "flexvnf_nic_2" {
    name                      = "FlexVNF_NIC2"
    location                  = var.location
    resource_group_name       = azurerm_resource_group.versa_rg.name
	enable_ip_forwarding      = "true"
	
    ip_configuration {
        name                          = "FlexVNF_NIC2_Configuration"
        subnet_id                     = azurerm_subnet.versa_tf_wan_subnet.id
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = azurerm_public_ip.ip_flexvnf_wan.id

    }
    tags = {
        environment = "VersaFlexVNF"
    }
}

# Create LAN network interface for FlexVNF
resource "azurerm_network_interface" "flexvnf_nic_3" {
    name                      = "FlexVNF_NIC3"
    location                  = var.location
    resource_group_name       = azurerm_resource_group.versa_rg.name
	enable_ip_forwarding      = "true"
	
    ip_configuration {
        name                          = "FlexVNF_NIC3_Configuration"
        subnet_id                     = azurerm_subnet.versa_tf_lan_subnet.id
        private_ip_address_allocation = "dynamic"
        # public_ip_address_id          = azurerm_public_ip.ip_flexvnf_wan.id

    }
    tags = {
        environment = "VersaFlexVNF"
    }
}


# Associate security group to Management Network Interface
resource "azurerm_network_interface_security_group_association" "mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.flexvnf_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_mgnt_network.id
}

# Associate security group to WAN Network Interface
resource "azurerm_network_interface_security_group_association" "wan_nic_nsg" {
  network_interface_id      = azurerm_network_interface.flexvnf_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_wan_network.id
}
# Associate security group to LAN Network Interface
resource "azurerm_network_interface_security_group_association" "lan_nic_nsg" {
  network_interface_id      = azurerm_network_interface.flexvnf_nic_3.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_lan_network.id
}


# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        resource_group = azurerm_resource_group.versa_rg.name
    }

    byte_length = 4
}

# Create storage account for boot diagnostics of FlexVNF VM
resource "azurerm_storage_account" "storageaccountFlexVNF" {
    name                        = "vnfdiag${random_id.randomId.hex}"
    resource_group_name         = azurerm_resource_group.versa_rg.name
    location                    = var.location
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "VersaFlexVNF"
    }
}

# Add template to use custom data for FlexVNF:
data "template_file" "user_data_flexvnf" {
  template = file("./initial_auto_staging.yaml")
  
  vars = {
    sshkey             = var.ssh_key
    master_dir_south_bound_ip = var.master_dir_south_bound_ip
    slave_dir_south_bound_ip = var.slave_dir_south_bound_ip
    controller_ip =  var.controller_ip
    wan_gateway = var.wan_gateway
    local_identifier = var.local_identifier
    remote_identifier = var.remote_identifier
    serial_number = var.serial_number
    vos_wan_ip = azurerm_network_interface.flexvnf_nic_2.private_ip_address
  }
}

data "template_cloudinit_config" "user_data_flexvnf" {
  gzip          = true
  base64_encode = true
  part {
    content_type = "text/cloud-config"
    content      = data.template_file.user_data_flexvnf.rendered
  }
}

# Create Versa FlexVNF Virtual Machine
resource "azurerm_virtual_machine" "flexVNF" {
    name                  = var.vm_name
    location              = var.location
    resource_group_name   = azurerm_resource_group.versa_rg.name
    depends_on            = [azurerm_network_interface_security_group_association.mgmt_nic_nsg, azurerm_network_interface_security_group_association.wan_nic_nsg,azurerm_network_interface_security_group_association.lan_nic_nsg]
    network_interface_ids = [azurerm_network_interface.flexvnf_nic_1.id, azurerm_network_interface.flexvnf_nic_2.id,azurerm_network_interface.flexvnf_nic_3.id]
	primary_network_interface_id = azurerm_network_interface.flexvnf_nic_1.id
    vm_size               = var.flexvnf_vm_size
	
    storage_os_disk {
        name              = "FlexVNF-1_OSDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        id  =  var.image_flexvnf
    }

    os_profile {
        computer_name  = "versa-flexvnf"
        admin_username = "versa_devops"
        custom_data    = data.template_file.user_data_flexvnf.rendered
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/versa_devops/.ssh/authorized_keys"
            key_data = var.ssh_key
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = azurerm_storage_account.storageaccountFlexVNF.primary_blob_endpoint
    }
	
    tags = {
        environment = "VersaFlexVNF"
    }
}


data "azurerm_public_ip" "flexvnf_mgnt_pub_ip" {
  name = azurerm_public_ip.ip_flexvnf_mgmt.name
  resource_group_name   = azurerm_resource_group.versa_rg.name
  depends_on = [azurerm_virtual_machine.flexVNF]
}

data "azurerm_public_ip" "flexvnf_wan_pub_ip" {
  name = azurerm_public_ip.ip_flexvnf_wan.name
  resource_group_name   = azurerm_resource_group.versa_rg.name
  depends_on = [azurerm_virtual_machine.flexVNF]
}