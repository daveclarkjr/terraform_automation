#!/bin/bash
log_path="/etc/bootLog.txt"
if [ -f "$log_path" ]
then
    echo "Cloud Init script already ran earlier during first time boot.." >> $log_path
    route add ${master_dir_mgmt_ip}/32 gw ${mgmt_subnet_gateway} dev eth0
    route add ${Versa_Analytics_1_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
    route add ${Versa_Analytics_2_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
    echo "exiting the script.."  >> $log_path
    exit    
else
    touch $log_path
SSHKey="${sshkey}"
DirIP_1="${master_dir_mgmt_ip}"
DirIP_2="${master_dir_south_bound_ip}"
slave_dir_mgmt_ip="${slave_dir_mgmt_ip}"
slave_dir_south_bound_ip="${slave_dir_south_bound_ip}"
Versa_Analytics_1_Instance_south_bound_IP="${Versa_Analytics_1_Instance_south_bound_IP}"
Versa_Analytics_2_Instance_south_bound_IP="${Versa_Analytics_2_Instance_south_bound_IP}"
Address="Match Address $DirIP_1,$DirIP_2"
KeyDir="/home/Administrator/.ssh"
KeyFile="/home/Administrator/.ssh/authorized_keys"
SSH_Conf="/etc/ssh/sshd_config"
UBUNTU_RELEASE="$(lsb_release -cs)"
echo "Starting cloud init script...." > $log_path

echo "Modifying /etc/network/interface file.." >> $log_path
cp /etc/network/interfaces /etc/network/interfaces.bak
if [[ $UBUNTU_RELEASE == "trusty" ]]; then
cat > /etc/network/interfaces << EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet dhcp
post-up route add -net 0.0.0.0/0 gw ${mgmt_subnet_gateway} dev eth0
post-up route add ${master_dir_mgmt_ip}/32 gw ${mgmt_subnet_gateway} dev eth0
post-up route add ${Versa_Analytics_1_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
post-up route add ${Versa_Analytics_2_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
# The secondary network interface
auto eth1
iface eth1 inet dhcp
post-up route add -net ${overlay_prefixes} gw ${dr_router_south_bound_ip} dev eth1
post-up route add -net ${DC_mgmt_subnet} gw ${dr_router_south_bound_ip} dev eth1
post-up route add -net ${DC_south_bound_network_subnet} gw ${dr_router_south_bound_ip} dev eth1
EOF
else
cat > /etc/network/interfaces << EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback
# The primary network interface
auto eth0
iface eth0 inet dhcp
    offload-gro off
    up route add -net 0.0.0.0/0 gw ${mgmt_subnet_gateway} dev eth0
    up route add ${master_dir_mgmt_ip}/32 gw ${mgmt_subnet_gateway} dev eth0
    up route add ${Versa_Analytics_1_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
    up route add ${Versa_Analytics_2_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
# The secondary network interface
auto eth1
iface eth1 inet dhcp
    offload-gro off
    up route add -net ${overlay_prefixes} gw ${dr_router_south_bound_ip} dev eth1
    up route add -net ${DC_mgmt_subnet} gw ${dr_router_south_bound_ip} dev eth1
    up route add -net ${DC_south_bound_network_subnet} gw ${dr_router_south_bound_ip} dev eth1
EOF
fi

echo -e "Modified /etc/network/interface file. Refer below new interface file content:\n`cat /etc/network/interfaces`" >> $log_path

echo "Restart Network services.." >> $log_path
if [[ $UBUNTU_RELEASE == "trusty" ]]; then
    /etc/init.d/networking restart >> /dev/null 2>&1
else
    systemctl restart networking >> /dev/null 2>&1
fi

echo "Modifying /etc/hosts file.." >> $log_path
cp /etc/hosts /etc/hosts.bak
cat > /etc/hosts << EOF
127.0.0.1       localhost
${slave_dir_south_bound_ip}     ${hostname_dir_master}
${master_dir_south_bound_ip}        ${hostname_dir_1}
# The following lines are desirable for IPv6 capable hosts cloudeinit
::1localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF
echo -e "Modified /etc/hosts file. Refer below new hosts file content:\n`cat /etc/hosts`" >> $log_path

echo "Moditing /etc/hostname file.." >> $log_path
hostname ${hostname_dir_master}
cp /etc/hostname /etc/hostname.bak
cat > /etc/hostname << EOF
${hostname_dir_master}
EOF
echo "Hostname modified to : `hostname`" >> $log_path

echo -e "Injecting ssh key into Administrator user.\n" >> $log_path
if [ ! -d "$KeyDir" ]; then
    echo -e "Creating the .ssh directory and injecting the SSH Key.\n" >> $log_path
    sudo mkdir $KeyDir
    sudo echo $SSHKey >> $KeyFile
    sudo chown Administrator:versa $KeyDir
    sudo chown Administrator:versa $KeyFile
    sudo chmod 600 $KeyFile
elif ! grep -Fq "$SSHKey" $KeyFile; then
    echo -e "Key not found. Injecting the SSH Key.\n" >> $log_path
    sudo echo $SSHKey >> $KeyFile
    sudo chown Administrator:versa $KeyDir
    sudo chown Administrator:versa $KeyFile
    sudo chmod 600 $KeyFile
else
    echo -e "SSH Key already present in file: $KeyFile.." >> $log_path
fi

echo -e "Enanbling ssh login using password." >> $log_path

echo -e "Generating director self signed certififcates. Refer detail below:\n" >> $log_path
sudo rm -rf /var/versa/vnms/data/certs/
sudo -u versa /opt/versa/vnms/scripts/vnms-certgen.sh --cn ${hostname_dir_master} --storepass versa123 >> $log_path
sudo chown -R versa:versa /var/versa/vnms/data/certs/

echo "Adding north bond and south bond interface in setup.json file.." >> $log_path
cat > /opt/versa/etc/setup.json << EOF
{
    "input":{
        "version": "1.0",     
        "south-bound-interface":[
          "eth1"
        ],
        "north-bound-interface":[
          "eth0"
        ],         
        "hostname": "${hostname_dir_master}",
        "disable-reset-password-for-first-time-user-login":"yes"
     }
}
EOF
volume_extend_log_path="/tmp/volume_extend_Log.txt"
hd=$(lsblk | grep 200G | awk  '{print $1}')
if [ -f "\$volume_extend_log_path" ]
then
    echo "volume extension ran earlier.." >> $volume_extend_log_path
else
    (echo n; echo p; echo 3; echo 314570752; echo 419430398; echo t; echo 3; echo 8e; echo w) | fdisk /dev/$hd >> $volume_extend_log_path
    partprobe
    pvcreate /dev/$hd'3' >> $volume_extend_log_path
    vgextend system /dev/$hd'3' >> $volume_extend_log_path
    lvextend -l+100%FREE /dev/system/root >> $volume_extend_log_path
    resize2fs /dev/mapper/system-root >> $volume_extend_log_path    
fi
echo -e "Got below data from setup.json file:\n `cat /opt/versa/etc/setup.json`" >> $log_path
echo -e "Enanbling ssh login using password." >> $log_path
if ! grep -Fq "$Address" $SSH_Conf; then
    echo -e "Adding the match address exception for Director Management IP.\n" >> $log_path
    sed -i.bak "\$a\Match Address $DirIP_1,$DirIP_2\n  PasswordAuthentication yes\nMatch all" $SSH_Conf
    sed -i.bak "\$a\Match Address $slave_dir_mgmt_ip,$slave_dir_south_bound_ip\n  PasswordAuthentication yes\nMatch all" $SSH_Conf
    sed -i.bak "\$a\Match Address $Versa_Analytics_1_Instance_south_bound_IP,$Versa_Analytics_2_Instance_south_bound_IP\n  PasswordAuthentication yes\nMatch all" $SSH_Conf
    sudo service ssh restart
else
    echo -e "Director Management IP address is alredy present in file $SSH_Conf.\n" >> $log_path
fi
echo "Add DC managment route addition" >> $log_path
route add ${master_dir_mgmt_ip}/32 gw ${mgmt_subnet_gateway}
route add ${Versa_Analytics_1_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
route add ${Versa_Analytics_2_Instance_south_bound_IP}/32 gw ${mgmt_subnet_gateway} dev eth0
echo "Executing the startup script in non interactive mode.." >> $log_path
sleep 60
sudo -u Administrator /opt/versa/vnms/scripts/vnms-startup.sh --non-interactive >> /dev/null 2>&1
fi