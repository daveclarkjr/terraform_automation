# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "versa_rg" {
  name     = var.resource_group
  location = var.location

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Create virtual network
resource "azurerm_virtual_network" "versaNetwork" {
  name                = "Versa_VPC"
  address_space       = [var.vpc_address_space]
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management Subnet
resource "azurerm_subnet" "mgmt_subnet" {
  name                 = "MGMT-NET"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.mgmt_subnet]
}

# Create Traffic Subnet for Director, ServiceVNF and Analytics
resource "azurerm_subnet" "south_bound_network_subnet" {
  name                 = "south_bound-Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.south_bound_network_subnet]
}
# Create Traffic Subnet for controller, ServiceVNF
resource "azurerm_subnet" "control_network_subnet" {
  name                 = "control_network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.control_network_subnet]
}
# Create Interconnection Traffic Subnet for SVNF and SVNF
resource "azurerm_subnet" "inter_network_subnet" {
  name                 = "INTER-Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.inter_network_subnet]
}

# Create WAN transport Traffic Subnet for Controller and Branch
resource "azurerm_subnet" "wan_network_subnet" {
  name                 = "WAN-Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.internet_subnet]
}
# Create Route Table
resource "azurerm_route_table" "mgnt_south_bound_network_route_table" {
  name                = "mgnt_south_bound_network_route_table"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  depends_on           = [azurerm_network_interface.svnf_nic_2]
}

resource "azurerm_route_table" "control_network_route_table" {
  name                = "control_network_route_table"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  depends_on           = [azurerm_network_interface.controller_nic_2,azurerm_network_interface.svnf_nic_4]
}
resource "azurerm_route_table" "inter_network_route_table" {
  name                = "inter_network_route_table"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
}
# Associate Route Table to Subnet
resource "azurerm_subnet_route_table_association" "south_bound_network_route_table_association" {
  subnet_id      = azurerm_subnet.south_bound_network_subnet.id
  route_table_id = azurerm_route_table.mgnt_south_bound_network_route_table.id
}
resource "azurerm_subnet_route_table_association" "mgnt_ntw_route_table_association" {
  subnet_id      = azurerm_subnet.mgmt_subnet.id
  route_table_id = azurerm_route_table.mgnt_south_bound_network_route_table.id
}
resource "azurerm_subnet_route_table_association" "control_network_route_table_association" {
  subnet_id      = azurerm_subnet.control_network_subnet.id
  route_table_id = azurerm_route_table.control_network_route_table.id
}
resource "azurerm_subnet_route_table_association" "inter_network_route_table_association" {
  subnet_id      = azurerm_subnet.inter_network_subnet.id
  route_table_id = azurerm_route_table.inter_network_route_table.id
}
# Add Route in Mgnt Network Route Table

resource "azurerm_route" "DEFAULT_ROUTE_INET" {
  name                   = "DEFAULT_ROUTE_INET"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.mgnt_south_bound_network_route_table.name
  address_prefix         = "0.0.0.0/0"
  next_hop_type          = "Internet"
}
resource "azurerm_route" "DEFAULT_ROUTE_INTER_NTW" {
  name                   = "DEFAULT_ROUTE_INTER_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.inter_network_route_table.name
  address_prefix         = "0.0.0.0/0"
  next_hop_type          = "Internet"
}

# Add Route in South Bound Network Route Table
resource "azurerm_route" "DEFAULT_ROUTE_SOUTH_BOUND" {
  name                   = "DEFAULT_ROUTE_SOUTH_BOUND"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.mgnt_south_bound_network_route_table.name
  address_prefix         =var.overlay_prefixes
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.svnf_nic_2.private_ip_address
}
# Add Route in control Network Route Table
resource "azurerm_route" "DEFAULT_ROUTE_CONTROL_NTW" {
  name                   = "DEFAULT_ROUTE_CONTROL_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.control_network_route_table.name
  address_prefix         = "0.0.0.0/0"
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.controller_nic_2.private_ip_address
}
resource "azurerm_route" "DC_SOUTH_BOUND_NTW_FOR_CTRL_NTW" {
  name                   = "DC_SOUTH_BOUND_NTW_FOR_CTRL_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.control_network_route_table.name
  address_prefix         = var.DC_south_bound_network_subnet
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.svnf_nic_4.private_ip_address
}
resource "azurerm_route" "DC_MGNT_NTW_FOR_CTRL_NTW" {
  name                   = "DC_MGNT_NTW_FOR_CTRL_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.control_network_route_table.name
  address_prefix         = var.DC_mgmt_subnet
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.svnf_nic_4.private_ip_address
}

resource "azurerm_route" "DC_SOUTH_BOUND_NTW" {
  name                   = "DC_SOUTH_BOUND_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.mgnt_south_bound_network_route_table.name
  address_prefix         = var.DC_south_bound_network_subnet
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.svnf_nic_2.private_ip_address
}
resource "azurerm_route" "DC_MGNT_NTW" {
  name                   = "DC_MGNT_NTW"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.mgnt_south_bound_network_route_table.name
  address_prefix         = var.DC_mgmt_subnet
  next_hop_type          = "VirtualAppliance"
  next_hop_in_ip_address = azurerm_network_interface.svnf_nic_2.private_ip_address
}

# Create Public IP for Director
resource "azurerm_public_ip" "ip_dir" {
  count               = length(var.hostname_director)
  name                = "PublicIP_Director_${1 + count.index}"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Public IP for Controller
# resource "azurerm_public_ip" "ip_controller" {
#   count               = length(var.hostname_director)
#   name                = "PublicIP_Controller_${1 + count.index}"
#   location            = var.location
#   resource_group_name = azurerm_resource_group.versa_rg.name
#   sku                 = "Standard"
#   allocation_method   = "Static"
#   availability_zone = var.availability_zone

#   tags = {
#     environment = "${var.tag_name}_VersaHeadEnd"
#   }
# }

# Create Public IP for SVNF
# resource "azurerm_public_ip" "ip_svnf" {
#   count               = length(var.hostname_director)
#   name                = "PublicIP_SVNF_${1 + count.index}"
#   location            = var.location
#   resource_group_name = azurerm_resource_group.versa_rg.name
#   sku                 = "Standard"
#   allocation_method   = "Static"
#   availability_zone = var.availability_zone

#   tags = {
#     environment = "${var.tag_name}_VersaHeadEnd"
#   }
# }
# Create Public IP for SVNF Internet Interface
resource "azurerm_public_ip" "ip_svnf_inet" {
  count               = length(var.hostname_director)
  name                = "PublicIP_SVNF_${1 + count.index}_INET"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Public IP for Controller Internet Interface
resource "azurerm_public_ip" "ip_controller_inet" {
  count               = length(var.hostname_director)
  name                = "PublicIP_Controller_${1 + count.index}_INET"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# # Create Public IP for Controller WAN Interface
# resource "azurerm_public_ip" "ip_controller_wan" {
#   count               = length(var.hostname_director)
#   name                = "PublicIP_Controller_${1 + count.index}_WAN"
#   location            = var.location
#   resource_group_name = azurerm_resource_group.versa_rg.name
#   sku                 = "Standard"
#   allocation_method   = "Static"
#   availability_zone = var.availability_zone

#   tags = {
#     environment = "${var.tag_name}_VersaHeadEnd"
#   }
# }

# Create Network Security Groups and rules for Director
resource "azurerm_network_security_group" "versa_nsg_dir" {
  name                = "VersaDir-NSG"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  security_rule {
    name                       = "Versa_Security_Rule_TCP"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "4566", "4570", "5432", "443", "9182-9183", "2022", "4949", "20514", "6080", "9090"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_UDP"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["20514"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_Outbound"
    priority                   = 251
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Network Security Groups and rules for FlexVNF
resource "azurerm_network_security_group" "versa_nsg_vnf" {
  name                = "VersaFlexVNF-NSG"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  security_rule {
    name                       = "Versa_Security_Rule_TCP"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "2022", "1024-1120", "3000-3003", "9878", "8443", "5201"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_UDP"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["500", "3002-3003", "4500", "4790"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_Outbound"
    priority                   = 251
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_ESP"
    priority                   = 301
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_address_prefix      = "VirtualNetwork"
    destination_address_prefix = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for Director
resource "azurerm_network_interface" "director_nic_1" {
  count                = length(var.hostname_director)
  name                 = "Director_${1 + count.index}_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_subnet.mgmt_subnet]
  ip_configuration {
    name                          = "Director_${1 + count.index}_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_dir_mgnt_interfaces_IP[0]
    public_ip_address_id          = azurerm_public_ip.ip_dir[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound network interface for Director
resource "azurerm_network_interface" "director_nic_2" {
  count                = length(var.hostname_director)
  name                 = "Director_${1 + count.index}_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_subnet.south_bound_network_subnet]
  ip_configuration {
    name                          = "Director_${1 + count.index}_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_south_bound_network_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for Controller
resource "azurerm_network_interface" "controller_nic_1" {
  name                 = "Controller_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.svnf_nic_1]
  ip_configuration {
    name                          = "Controller_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_controller_flex_mgnt_interfaces_IP[1]
    # public_ip_address_id          = azurerm_public_ip.ip_controller[0].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Northbound/Control network interface for Controller
resource "azurerm_network_interface" "controller_nic_2" {
  name                 = "Controller_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.controller_nic_1,azurerm_network_interface.svnf_nic_4]
  ip_configuration {
    name                          = "Controller_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.control_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_control_network_interface_IP[1]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/INTERNET network interface for Controller
resource "azurerm_network_interface" "controller_nic_3" {
  name                 = "Controller_NIC3"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.controller_nic_2]
  ip_configuration {
    name                          = "Controller_NIC3_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_internet_subnet_interface_IP[0]
    public_ip_address_id          = azurerm_public_ip.ip_controller_inet[0].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/WAN network interface for Controller
resource "azurerm_network_interface" "controller_nic_4" {
  name                 = "Controller_NIC4"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.controller_nic_3]
  ip_configuration {
    name                          = "Controller_NIC4_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_internet_subnet_interface_IP[1]
    # public_ip_address_id          = azurerm_public_ip.ip_controller_wan[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_1" {
  name                 = "Svnf_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.director_nic_1]
  ip_configuration {
    name                          = "Svnf_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_controller_flex_mgnt_interfaces_IP[0]
    # public_ip_address_id          = azurerm_public_ip.ip_svnf[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_2" {
  name                 = "Svnf_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.svnf_nic_1,azurerm_network_interface.director_nic_2]
  ip_configuration {
    name                          = "Svnf_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_south_bound_network_interfaces_IP[1]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_3" {
  name                 = "Svnf_NIC3"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.svnf_nic_2]
  ip_configuration {
    name                          = "Svnf_NIC3_Configuration"
    subnet_id                     = azurerm_subnet.inter_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_inter_network_interface_IP[0]
    public_ip_address_id          = azurerm_public_ip.ip_svnf_inet[0].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_4" {
  name                 = "Svnf_NIC4"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"
  depends_on           = [azurerm_network_interface.svnf_nic_3]
  ip_configuration {
    name                          = "Svnf_NIC4_Configuration"
    subnet_id                     = azurerm_subnet.control_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.DR_control_network_interface_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Associate security group to Director Management Network Interface
resource "azurerm_network_interface_security_group_association" "dir_mgmt_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.director_nic_1[count.index].id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}

# Associate security group to Director Southbound Network Interface
resource "azurerm_network_interface_security_group_association" "dir_sb_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.director_nic_2[count.index].id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}

# Associate security group to Controller Management Network Interface
resource "azurerm_network_interface_security_group_association" "controller_mgmt_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.controller_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller Control Network Interface
resource "azurerm_network_interface_security_group_association" "controller_sb_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.controller_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller Internet Network Interface
resource "azurerm_network_interface_security_group_association" "controller_inet_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.controller_nic_3.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller WAN Network Interface
resource "azurerm_network_interface_security_group_association" "controller_wan_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.controller_nic_4.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to SVNF Management Network Interface
resource "azurerm_network_interface_security_group_association" "svnf_mgmt_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.svnf_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to SVNF Control Network Interface
resource "azurerm_network_interface_security_group_association" "svnf_nb_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.svnf_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to SVNF Interconnection Interface
resource "azurerm_network_interface_security_group_association" "svnf_inter_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.svnf_nic_3.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}
# Associate security group to SVNF Connects controller
resource "azurerm_network_interface_security_group_association" "svnf_control_nic_nsg" {
  count                     = length(var.hostname_director)
  network_interface_id      = azurerm_network_interface.svnf_nic_4.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}
# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    resource_group = azurerm_resource_group.versa_rg.name
  }

  byte_length = 4
}

# Create storage account for boot diagnostics of Director VM
resource "azurerm_storage_account" "storageaccountDir" {
  count                    = length(var.hostname_director)
  name                     = "dir${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create storage account for boot diagnostics of Controller VM
resource "azurerm_storage_account" "storageaccountcontroller" {
  count                    = length(var.hostname_director)
  name                     = "controller${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create storage account for boot diagnostics of ServiceVNF VM
resource "azurerm_storage_account" "storageaccountSVNF" {
  count                    = length(var.hostname_director)
  name                     = "svnf${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Add template to use custom data for Director:
data "template_file" "user_data_dir" {
  count    = length(var.hostname_director)
  template = file("./versa_he_500_cpe_multiregion_dr/slave_director.sh")

  vars = {
    hostname_dir_master = var.hostname_director[0]
    sshkey              = var.ssh_key
    hostname_dir_1 = var.hostname_dir_1[0]
    mgmt_subnet_gateway = var.mgmt_subnet_gateway    
    master_dir_mgmt_ip  = var.master_dir_mgmt_ip
    master_dir_south_bound_ip = var.master_dir_south_bound_ip
    slave_dir_mgmt_ip = azurerm_network_interface.director_nic_1[0].private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.director_nic_2[0].private_ip_address
    DC_mgmt_subnet = var.DC_mgmt_subnet
    DC_south_bound_network_subnet = var.DC_south_bound_network_subnet
    dr_router_south_bound_ip = azurerm_network_interface.svnf_nic_2.private_ip_address
    overlay_prefixes = var.overlay_prefixes
    Versa_Analytics_1_Instance_south_bound_IP = var.Versa_Analytics_1_Instance_south_bound_IP
    Versa_Analytics_2_Instance_south_bound_IP = var.Versa_Analytics_2_Instance_south_bound_IP
  }
}

# Create Versa Director Virtual Machine
resource "azurerm_virtual_machine" "directorVM" {
  count                        = length(var.hostname_director)
  name                         = "Versa_Director_${1 + count.index}"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.dir_mgmt_nic_nsg, azurerm_network_interface_security_group_association.dir_sb_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.director_nic_1[count.index].id, azurerm_network_interface.director_nic_2[count.index].id]
  primary_network_interface_id = azurerm_network_interface.director_nic_1[count.index].id
  vm_size                      = var.director_vm_size

  storage_os_disk {
    name              = "Director_${1 + count.index}_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = var.dir_disk_size
  }

  storage_image_reference {
    id = var.image_director
  }

  os_profile {
    computer_name  = var.hostname_director[count.index]
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_dir[count.index].rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountDir[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Add template to use custom data for Controller:
data "template_file" "user_data_controller" {
  # count    = length(var.hostname_director)
  template = file("./versa_he_500_cpe_multiregion_dr/controller.sh")

  vars = {
    sshkey             = var.ssh_key
    master_dir_mgmt_ip  = var.master_dir_mgmt_ip
    master_dir_south_bound_ip = var.master_dir_south_bound_ip
    slave_dir_mgmt_ip = azurerm_network_interface.director_nic_1[0].private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.director_nic_2[0].private_ip_address    
  }
}

# data "template_cloudinit_config" "user_data_controller" {
#   gzip          = true
#   base64_encode = true
#   part {
#     content_type = "text/cloud-config"
#     content      = data.template_file.user_data_controller.rendered
#   }
# }

# Create Versa Controller Virtual Machine
resource "azurerm_virtual_machine" "controllerVM" {
  count                        = length(var.hostname_director)
  name                         = "Versa_Controller_${1 + count.index}"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.controller_mgmt_nic_nsg, azurerm_network_interface_security_group_association.controller_sb_nic_nsg, azurerm_network_interface_security_group_association.controller_inet_nic_nsg, azurerm_network_interface_security_group_association.controller_wan_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.controller_nic_1.id, azurerm_network_interface.controller_nic_2.id, azurerm_network_interface.controller_nic_3.id, azurerm_network_interface.controller_nic_4.id]
  primary_network_interface_id = azurerm_network_interface.controller_nic_1.id
  vm_size                      = var.controller_vm_size

  storage_os_disk {
    name              = "Controller_${1 + count.index}_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = var.controller_disk_size
  }

  storage_image_reference {
    id = var.image_controller
  }

  os_profile {
    computer_name  = "versa-flexvnf"
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_controller.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountcontroller[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Add template to use custom data for ServiceVNF:
data "template_file" "user_data_svnf" {
  # count    = length(var.hostname_director)
  template = file("./versa_he_500_cpe_multiregion_dr/dr_dc_network_config_gen.yaml")

  vars = {
    sshkey             = var.ssh_key
    parent_org_name = var.parent_org_name
    South_Bound_Network_ip = azurerm_network_interface.svnf_nic_2.private_ip_address
    internet_interface_ip  = azurerm_network_interface.svnf_nic_3.private_ip_address
    DC_internet_public_IP = var.DC_internet_public_IP
    DC_internet_private_IP = var.DC_internet_private_IP
    master_dir_mgmt_ip  = var.master_dir_mgmt_ip
    master_dir_south_bound_ip = var.master_dir_south_bound_ip
    slave_dir_mgmt_ip = azurerm_network_interface.director_nic_1[0].private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.director_nic_2[0].private_ip_address
    DC_mgmt_subnet = var.DC_mgmt_subnet
    DR_mgmt_subnet = azurerm_subnet.mgmt_subnet.address_prefix
    controller_2_south_bound_ip = azurerm_network_interface.controller_nic_2.private_ip_address
    inter_network_subnet_gateway = var.inter_network_subnet_gateway
    dr_router_control_ntw_ip = azurerm_network_interface.svnf_nic_4.private_ip_address  
    dr_south_bound_network_gateway = var.dr_south_bound_network_gateway 
  }
}
data "template_cloudinit_config" "user_data_dr_router_config" {
  gzip          = true
  base64_encode = true
  part {
    content_type = "text/cloud-config"
    content      = data.template_file.user_data_svnf.rendered
  }
}

# Create Versa SVNF Virtual Machine
resource "azurerm_virtual_machine" "svnfVM" {
  count                        = length(var.hostname_director)
  name                         = "Versa_SVNF_${1 + count.index}"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.svnf_mgmt_nic_nsg, azurerm_network_interface_security_group_association.svnf_nb_nic_nsg, azurerm_network_interface_security_group_association.svnf_inter_nic_nsg, azurerm_network_interface_security_group_association.svnf_control_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.svnf_nic_1.id, azurerm_network_interface.svnf_nic_2.id, azurerm_network_interface.svnf_nic_3.id, azurerm_network_interface.svnf_nic_4.id]
  primary_network_interface_id = azurerm_network_interface.svnf_nic_1.id
  vm_size                      = var.svnf_vm_size

  storage_os_disk {
    name              = "Svnf_${1 + count.index}_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_svnf
  }

  os_profile {
    computer_name  = "versa-flexvnf"
    admin_username = "versa_devops"
    custom_data    = data.template_cloudinit_config.user_data_dr_router_config.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountSVNF[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


